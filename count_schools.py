import csv

#call in main
filename = "school_data.csv"

class CountSchools:
    def __init__(self, filename=None):
        self.filename = filename
        self.fields = []
        self.rows = []
        self.states = {}
        self.metro = {}
        self.city = {}
        self.open_file()
    
    def open_file(self):
        with open (self.filename, newline='',encoding='ISO-8859-1') as csvfile:
            csvreader = csv.reader(csvfile, delimiter=',')
    
            self.fields = next(csvreader)

            for row in csvreader:
                self.rows.append(row)

    def print_counts(self):
        total_schools = len(self.rows)
        print ("Total Schools:",total_schools)
        for i in range(len(self.rows)):
            if self.rows[i][5] not in self.states:
                self.states[self.rows[i][5]] = 1
            else:
                self.states[self.rows[i][5]] += 1

            if self.rows[i][8] not in self.metro:
                self.metro[self.rows[i][8]] = 1
            else:
                self.metro[self.rows[i][8]] += 1

            if self.rows[i][4] not in self.city:
                self.city[self.rows[i][4]] = 1
            else:
                self.city[self.rows[i][4]] += 1

        print ("Schools by state:")
        for key in self.states:
            print ('%s : %s'%(key,self.states[key]))

        print ("Schools by metro centric locale:")
        for key in self.metro:
            print ('%s : %s'%(key, self.metro[key]))

        max_school = 0
        city = ""
        city_count = 0
        for key in self.city:
            if self.city[key] > max_school:
                max_school = self.city[key]
                city = key

            if self.city[key] != 'N/A':
                city_count += 1

        print ("City with most schools is: %s"%(city))
        print ("The number of schools %s has is %s"%(city,max_school))
        print ("%s unique cities have atleast one school in it"%city_count)


def main():      
    countschools = CountSchools("school_data.csv")
    countschools.print_counts()

if __name__ == "__main__":
    main()
