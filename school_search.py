import csv
import time
import sys
import re
import math
from collections import Counter

WORD = re.compile(r'\w+')

filename = "school_data.csv"

class SchoolSearch:
    def __init__(self, filename=None):
        self.filename = filename
        self.rows = []
        self.fields = []
        self.vectors = {}
        self.open_file()

    
    def open_file(self):
        with open (self.filename, newline='',encoding='ISO-8859-1') as csvfile:
            csvreader = csv.reader(csvfile, delimiter=',')
    
            self.fields = next(csvreader)

            for row in csvreader:
                self.rows.append(row)
        for i in range(len(self.rows)):
            address = str(self.rows[i][3])+' '+str(self.rows[i][4])+' '+str(self.rows[i][5])
            self.vectors[address] = self.text_to_vector(address)

    def get_cosine(self, vec1, vec2):
        intersection = set(vec1.keys()) & set(vec2.keys())
        numerator = sum([vec1[x] * vec2[x] for x in intersection])

        sum1 = sum([vec1[x]**2 for x in vec1.keys()])
        sum2 = sum([vec2[x]**2 for x in vec2.keys()])
        denominator = math.sqrt(sum1) * math.sqrt(sum2)

        if not denominator:
            return 0.0
        else:
            return float(numerator) / denominator

    def text_to_vector(self, text):
        words = WORD.findall(text)
        return Counter(words)
        

    def search(self, s, minCost=.3):
        results = []
        cosine_dict = {}
        input_vector = self.text_to_vector(s.upper())
        for address,vector in self.vectors.items():
            cosine = self.get_cosine(input_vector, vector)
            if cosine >= minCost:
                if not cosine_dict.get(cosine):
                    cosine_dict[cosine] = [address]
                else:
                    cosine_dict[cosine] = cosine_dict[cosine]+[address]
        for key in sorted(cosine_dict.keys(),reverse=True):
            for address in cosine_dict[key]:
                results.append(address)
                if len(results) >= 3:
                    return results
        return results

def main():      
    searchschools = SchoolSearch("school_data.csv")
    start =  time.time()
    results = searchschools.search('elementary school highland park')
    end = time.time()
    print ('time is %.5ss'%(end-start))
    print (results)
if __name__ == "__main__":
    main()